import { combineReducers } from 'redux';

import pizzaReducer from './pizzas/pizzas.reducer';
import signInReducer from './sign-in/sign-in.reducer';
import signUpReducer from './sign-up/sign-up.reducer';
import cartReducer from './cart/cart.reducer';
import userReducer from './user/user.reducer';

export default combineReducers({
  pizzas: pizzaReducer,
  signIn: signInReducer,
  signUp: signUpReducer,
  cart: cartReducer,
  user: userReducer,
});
