import signUpActionTypes from './sign-up.types';

export const setSignUpUser = (signUpUser) => ({
  type: signUpActionTypes.SET_SIGN_UP_USER,
  payload: signUpUser,
});
