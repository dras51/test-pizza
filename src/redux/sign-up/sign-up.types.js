const signUpActionTypes = {
  SET_SIGN_UP_USER: 'SET_SIGN_UP_USER',
};

export default signUpActionTypes;
