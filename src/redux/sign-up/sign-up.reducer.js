import signUpActionTypes from './sign-up.types';

const INITIAL_STATE = {
  displayName: '',
  email: '',
  password: '',
  confirmPassword: '',
};

const signUpReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case signUpActionTypes.SET_SIGN_UP_USER:
      return {
        ...state,
        signUpUser: action.payload,
      };
    default:
      return state;
  }
};

export default signUpReducer;
