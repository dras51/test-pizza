import pizzasActionTypes from './pizzas.types';

import { addPriceToPizzas } from './pizzas.utils';

const INITIAL_STATE = {
  pizzas: [],
};

const pizzaReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case pizzasActionTypes.ADD_PIZZAS:
      return {
        ...state,
        pizzas: action.payload.map((pizza) => addPriceToPizzas(pizza)),
      };
    default:
      return state;
  }
};

export default pizzaReducer;
