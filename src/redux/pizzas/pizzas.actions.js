import pizzasActionTypes from './pizzas.types';

export const addPizzas = (pizzas) => ({
  type: pizzasActionTypes.ADD_PIZZAS,
  payload: pizzas,
});
