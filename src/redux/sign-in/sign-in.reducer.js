import signInActionTypes from './sign-in.types';

const INITIAL_STATE = {
  signInUser: {
    email: '',
    password: '',
  },
};

const signInReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case signInActionTypes.SET_SIGN_IN_USER:
      return {
        ...state,
        signInUser: action.payload,
      };
    default:
      return state;
  }
};

export default signInReducer;
