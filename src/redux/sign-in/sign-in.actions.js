import signInActionTypes from './sign-in.types';

export const setSignInUser = (signInUser) => {
  return {
    type: signInActionTypes.SET_SIGN_IN_USER,
    payload: signInUser,
  };
};
