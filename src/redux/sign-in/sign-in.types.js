const signInActionTypes = {
  SET_SIGN_IN_USER: 'SET_SIGN_IN_USER',
};

export default signInActionTypes;
