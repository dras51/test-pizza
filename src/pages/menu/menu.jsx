import React from 'react';
import ContentPreview from '../../components/content-preview/content-preview';
import { connect } from 'react-redux';

import './menu.styles.scss';
const MenuPage = () => (
  <div className="menu-page">
    <ContentPreview />
  </div>
);

export default connect(null)(MenuPage);
