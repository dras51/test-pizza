import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const firebaseConfig = {
  apiKey: 'AIzaSyAI0yI0P6L01mgob-nNxpYDjb665rLDtPE',
  authDomain: 'test-pizza-db.firebaseapp.com',
  databaseURL: 'https://test-pizza-db.firebaseio.com',
  projectId: 'test-pizza-db',
  storageBucket: 'test-pizza-db.appspot.com',
  messagingSenderId: '557369071204',
  appId: '1:557369071204:web:a88611ed556530f1e3f171',
  measurementId: 'G-JLZK3H9JC4',
};

export const createUserProfileDocument = async (userAuth, additionalData) => {
  if (!userAuth) return;

  const userRef = firestore.doc(`users/${userAuth.uid}`);

  const snapShot = await userRef.get();

  if (!snapShot.exists) {
    const { displayName, email } = await userAuth;
    const createdAt = new Date();

    try {
      await userRef.set({ displayName, email, createdAt, ...additionalData });
    } catch (err) {
      console.log('error creating user', err);
    }
  }
  return userRef;
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ promt: 'select_account' });

export const signInWithGoogle = () => {
  auth.signInWithPopup(provider);
};

export default firebase;
