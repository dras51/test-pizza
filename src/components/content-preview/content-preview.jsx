import React, { Component } from 'react';

import { connect } from 'react-redux';

import './content-preview.styles.scss';

import { addPizzas } from '../../redux/pizzas/pizzas.actions';
import ContentItem from '../content-item/content-item';
class ContentPreview extends Component {
  componentDidMount() {
    fetch(
      'https://api.spoonacular.com/food/menuItems/search?apiKey=1ef44c39f37a49d5a6ef29949efc4576&query=pizza&offset=10'
    )
      .then((res) => res.json())
      .then((data) => {
        this.props.addPizzas(data.menuItems);
      });
  }

  render() {
    const {
      pizzas: { pizzas },
    } = this.props;
    return (
      <div className="content-preview">
        <h1 className="title">PIZZAS</h1>
        <div className="preview">
          {pizzas
            .filter((item, idx) => idx < 8)
            .map((item) => (
              <ContentItem key={item.id} item={item} />
            ))}
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ pizzas }) => ({
  pizzas: pizzas,
});
const mapDispatchToProps = (dispatch) => ({
  addPizzas: (pizza) => dispatch(addPizzas(pizza)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ContentPreview);
