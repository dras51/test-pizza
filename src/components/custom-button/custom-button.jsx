import React from 'react';

import './custom-button.styles.scss';

const CustomButton = ({ children, inverted, isGoogleSignIn }) => (
  <button
    className={`${inverted ? 'inverted' : ''} ${
      isGoogleSignIn ? 'isGoogleSignIn' : ''
    } custom-button`}
  >
    {children}
  </button>
);

export default CustomButton;
