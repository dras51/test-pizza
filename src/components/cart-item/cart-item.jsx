import React from 'react';
import { connect } from 'react-redux';

import './cart-item.styles.scss';

const cartItem = ({ cart: { image, title, quantity, price } }) => (
  <div className="cart-item">
    <img src={image} alt="item" />
    <div className="item-details">
      <span className="name">{title}</span>
      <span className="price">
        {quantity} x ${price} = ${quantity * price}
      </span>
    </div>
  </div>
);

const mapStateToProps = ({ pizzas }) => ({
  pizzas,
});

export default connect(mapStateToProps)(cartItem);
