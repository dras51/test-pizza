import React, { Component } from 'react';
import { connect } from 'react-redux';

import { setSignInUser } from '../../redux/sign-in/sign-in.actions';

import FormInput from '../form-input/form-input.jsx';
import CustomButton from '../custom-button/custom-button';

import { signInWithGoogle, auth } from '../../firebase/firebase.utils';

import './sign-in.styles.scss';

class SignIn extends Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
    };
  }
  handleSubmit = async (event) => {
    event.preventDefault();
    const { email, password } = this.state;

    try {
      await auth.signInWithEmailAndPassword(email, password);
      setSignInUser({ email: '', password: '' });
    } catch (err) {
      console.log(err);
    }
  };

  handleChange = (event) => {
    const { value, name } = event.target;

    this.setState({ [name]: value });
  };
  render() {
    const { email, password } = this.props;
    return (
      <div className="sign-in">
        <h1>I already have an account</h1>
        <span>Sign in with your email and password</span>
        <form onSubmit={this.handleSubmit}>
          <FormInput
            label="Email"
            type="email"
            value={email}
            name="email"
            required
            placeholder="example@email.com"
            handleChange={this.handleChange}
          />
          <FormInput
            label="Password"
            type="password"
            value={password}
            name="password"
            required
            placeholder="Password"
            handleChange={this.handleChange}
          />
          <div className="buttons">
            <CustomButton className="buttons" type="submit">
              SIGN IN
            </CustomButton>
            <div onClick={signInWithGoogle}>
              <CustomButton isGoogleSignIn>SIGN IN WITH GOOGLE</CustomButton>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = ({ signIn: { signInUser } }) => {
  return {
    signInUser,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setSignInUser: (User) => dispatch(setSignInUser(User)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
