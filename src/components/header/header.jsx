import React from 'react';
import { connect } from 'react-redux';

import { Link } from 'react-router-dom';
import { ReactComponent as Logo } from '../../assets/pizza.svg';
import { auth } from '../../firebase/firebase.utils';

import CartDropdown from '../cart-dropdown/cart-dropdown';
import CartIcon from '../cart-icon/cart-icon.jsx';

import './header.styles.scss';

const Header = ({ hidden, currentUser }) => (
  <nav className="header">
    <Link className="logo-container" to="/">
      <Logo className="logo" />
      <h1 className="title">TEST PIZZA</h1>
    </Link>
    <div className="options">
      {currentUser ? (
        <div className="option" onClick={() => auth.signOut()} to="/signout">
          SIGN OUT
        </div>
      ) : (
        <Link className="option" to="/signin">
          SIGN IN
        </Link>
      )}
      <CartIcon />
    </div>
    {hidden ? null : <CartDropdown />}
  </nav>
);

const mapStateToProps = ({ user: { currentUser }, cart: { hidden } }) => ({
  currentUser,
  hidden,
});

export default connect(mapStateToProps)(Header);
