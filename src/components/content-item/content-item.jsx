import React from 'react';

import { connect } from 'react-redux';

import CustomButton from '../custom-button/custom-button';
import { addCartItem } from '../../redux/cart/cart.actions';
import './content-item.styles.scss';

const ContentItem = ({ item, addCartItem }) => {
  const { image, title, price } = item;

  return (
    <div className="content-container">
      <div className="content-item">
        <div className="figure">
          <div className="image" style={{ backgroundImage: `url(${image})` }} />
        </div>
        <span className="title">{title}</span>
        <span className="description">
          This is a test description of how the pizzas would be descibed on how
          properly served, and how many slices it would contain
        </span>
      </div>
      <div className="content-footer">
        <span className="price">{price}$</span>
        <div className="button" onClick={() => addCartItem(item)}>
          <CustomButton className="button" inverted>
            ORDER
          </CustomButton>
        </div>
      </div>
    </div>
  );
};
const mapDispatchToProps = (dispatch) => ({
  addCartItem: (item) => dispatch(addCartItem(item)),
});

export default connect(null, mapDispatchToProps)(ContentItem);
