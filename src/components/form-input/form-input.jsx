import React from 'react';

import './form-input.styles.scss';

const FormInput = ({
  label,
  width,
  noMargin,
  placeholder,
  handleChange,
  ...otherProps
}) => (
  <div className={`${noMargin ? 'no-margin' : ''} group`}>
    {label ? <h2 className="form-input-label">{label}</h2> : null}

    <input
      className="form-input"
      style={{
        width: `${width}px`,
      }}
      placeholder={placeholder}
      onChange={handleChange}
      {...otherProps}
    />
  </div>
);

export default FormInput;
