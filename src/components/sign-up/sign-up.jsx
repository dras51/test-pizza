import React, { Component } from 'react';

import { auth, createUserProfileDocument } from '../../firebase/firebase.utils';

import './sign-up.styles.scss';

import FormInput from '../form-input/form-input';
import CustomButton from '../custom-button/custom-button';

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displayName: '',
      email: '',
      password: '',
      confirmPassword: '',
    };
  }
  handleSubmit = async (event) => {
    event.preventDefault();

    const { displayName, email, password, confirmPassword } = this.state;

    if (password !== confirmPassword) {
      alert("Passwords don't match");
      return;
    }
    try {
      const { user } = await auth.createUserWithEmailAndPassword(
        email,
        password
      );
      await createUserProfileDocument(user, { displayName });

      this.setState({
        displayName: '',
        email: '',
        password: '',
        confirmPassword: '',
      });
    } catch (err) {
      console.log(err);
    }
  };

  handleChange = (e) => {
    const { name, value } = e.target;

    this.setState({ [name]: value });
  };

  render() {
    const { displayName, email, password, confirmPassword } = this.state;
    return (
      <div className="sign-up">
        <h1>I do not have an account yet</h1>
        <span>Sign up with your email and password</span>
        <form className="form-input" onSubmit={this.handleSubmit}>
          <FormInput
            label="Email"
            type="email"
            placeholder="example@email.com"
            name="email"
            value={email}
            required
            onChange={this.handleChange}
          />
          <FormInput
            label="Full Name"
            name="displayName"
            type="text"
            placeholder="John Doe"
            value={displayName}
            required
            onChange={this.handleChange}
          />
          <div className="passwords">
            <FormInput
              label="Password"
              type="password"
              name="password"
              placeholder="Password"
              value={password}
              required
              width="200"
              noMargin
              onChange={this.handleChange}
            />
            <FormInput
              label="Confirm Password"
              type="password"
              name="confirmPassword"
              placeholder="Confirm Password"
              value={confirmPassword}
              required
              width="200"
              noMargin
              onChange={this.handleChange}
            />
          </div>
          <CustomButton className="buttons">SIGN UP</CustomButton>
        </form>
      </div>
    );
  }
}

export default SignUp;
